"""An AWS Python Pulumi program"""
from src.ec2 import create_ec2_instances
from src.rds import create_rds_instances

create_ec2_instances()
create_rds_instances()