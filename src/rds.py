import pulumi
import pulumi_aws as aws
from src.scg import group

rdsSize = 'db.t2.micro'
rdsEngine = 'postgres'
rdsStorage = 20
config = pulumi.Config()


def create_rds_instances():
    db = aws.rds.Instance("db1",
                          allocated_storage=rdsStorage,
                          engine=rdsEngine,
                          instance_class=rdsSize,
                          name=config.require('db1Name'),
                          password=config.require_secret('db1Password'),
                          username=config.require('db1Name'),
                          vpc_security_group_ids=[group.id],
                          skip_final_snapshot=True
                          )
