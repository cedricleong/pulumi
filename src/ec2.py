import pulumi
import pulumi_aws as aws
from src.scg import group

ec2Size = 't2.micro'

def create_ec2_instances():
    ami = aws.get_ami(
        most_recent="true",
        owners=["137112412989"],
        filters=[{"name": "name", "values": ["amzn-ami-hvm-*"]}]
    )

    user_data = """
    #!/bin/bash
    echo "Hello, World!" > index.html
    nohup python -m SimpleHTTPServer 80 &
    """

    server = aws.ec2.Instance('web1',
                              instance_type=ec2Size,
                              vpc_security_group_ids=[group.id],  # reference security group from above
                              user_data=user_data,
                              ami=ami.id)

    pulumi.export('publicIp', server.public_ip)
    pulumi.export('publicHostName', server.public_dns)