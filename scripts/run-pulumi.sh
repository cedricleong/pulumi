#!/bin/bash

# exit if a command returns a non-zero exit code and also print the commands and their args as they are executed
set -e -x

# Add the pulumi CLI to the PATH
export PATH=$PATH:$HOME/.pulumi/bin

# Learn more about pulumi configuration at: https://www.pulumi.com/docs/intro/concepts/config/
pulumi stack select dev
pulumi config set aws:region us-east-1
pulumi config set db1Name CIDB
pulumi config set db1Username CIDB
pulumi config set --secret db1Password CIPassword

pulumi up --yes
pulumi destroy --yes